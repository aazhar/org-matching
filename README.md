# org-matching

L'algorithme de correspondance automatiques des affiliations.

# Approche actuelle d'affiliation des auteurs

Lors du dépôt et dans le cas de dépôt par fichier ou à l'aide d'un identifiant fort (DOI, PUBMEDID ...), des métadonnées sont récupérées à partir de services externes comme crossref, pubmed ou bien extraites si dépôt par fichier, parmis ces metadonnées on a des informations sur les auteurs , des fois leurs affiliations et l'année de publication..

Afin de proposer une affiliation depuis le référentiel structure pour **un auteur donné** (on ne cherche pas que l'affiliation car produit beaucoup d'erreurs mais l'auteur), on fait un appel à l'api HAL en **post** : **/ref/affiliation**, et dans le body on met la requete en json.

Les parametres / champs acceptes : ```['person_i', 'lastName_t', 'firstName_t',  'middleName_t', 'email_s', 'authId_i', 'structure_t', 'structName_t', 'structStatus_s', 'structCountry_s', 'structAddress_t', 'structType_s', 'structAcronym_s', 'structId_i', 'keyword_t', 'producedDate_s', 'country_s'];```

Pour trouver des correspondances à partir de ces données, on distingue deux cas A) recherche avec des informations sur l'auteur uniquement (+les metas) et B) recherche avec des informations sur l'auteur et son affiliation (+ les metas) et on procéde de cette façon (dans les requetes SOLR en remplaçant ce qui est entre crochet par la valeur désignée ):

    A) SI RECHERCHE AVEC UNIQUEMENT AUTEUR SANS AUCUNES INFOS SUR LES AFFILIATIONS

        RECHERCHE DE DEPOT PUBLIE (AVEC SI FOURNIE UNE DATE DE PUBLI LA PLUS PROCHE ) :

            A.1) RECHERCHER PAR NOM/PRENOM (ET TRIER PAR DIFF PAR RAPPORT A ANNEE DE PUBLI)
            
                ==> REQUETE SOLR EFFECTUEE SUR LE CORE "hal":

                    q=status_i%3A11
                    &fl=halId_s%2CauthFullName_s%2CauthLastName_s%2CauthFirstName_s%2Clabel_xml%2Csub%28producedDateY_i%2C2020%29%2Cabs%28sub%28producedDateY_i%2C[ANNEE DE PUBLI]%29%29
                    &fq=authLastName_sci%3A%22[PRENOM AUTEUR]%22+AND+authFirstName_sci%3A%22[NOM AUTEUR]%22
                    Si date publication dans les metas de recherchees :
                        &sort=abs%28sub%28producedDateY_i%2C[ANNEE DE PUBLI]%29%29+asc%2Csub%28producedDateY_i%2C[ANNEE DE PUBLI]%29+asc%2CproducedDate_tdate+desc
                                
                SI PAS DE RESULTATS , TERMINE
    
                SINON LE PREMIER RESULTAT EST RETOURNE
                
    B) SI RECHERCHE AVEC AUTEUR , SON AFFILIATION et LES METAS (Pour l instant annee de publi est prise en consideration) :

        POUR CHACUNE DES AFFILIATIONS DANS LES PARAMETRES

            RECHERCHE DE DEPOT DANS LE CORE SOLR "hal" :
    			
                B.1) RECHERCHE (fulltext) PAR NOM/PRENOM , FILTRE PAR INFO DE LA STRUCTURE ET TRIER PAR DIFF PAR RAPPORT A ANNEE DE PUBLI
                
                   ==> REQUETE SOLR EFFECTUE SUR LE CORE "hal":

                        q=authLastName_sci%3A%22[NOM AUTEUR]%22+AND+authFirstName_sci%3A%22[PRENOM AUTEUR]%22
                        Si date publication dans les metas de recherche :
                            sort=abs%28sub%28producedDateY_i%2C[ANNEE DE PUBLI]%29%29+asc%2Csub%28producedDateY_i%2C[ANNEE DE PUBLI]%29+asc%2CproducedDate_tdate+desc
                            &fl=%2Csub%28producedDateY_i%2C[ANNEE DE PUBLI]%29%2Cabs%28sub%28producedDateY_i%2C[ANNEE DE PUBLI]%29%29
                        Si on a un struct id dans les parametres de recherche!
                            &fq=structId_i%3A[STRUCT ID]
                        Sinon (avec plusieurs combinaisons selon les chaines de carateres proches de l'acronyme ou du nom..)
                            &fq=%28structAcronym_sci%3A%22[ACRONYME DE LE STRUCTURE]%22+OR+structName_sci%3A%22[NOM DE LA STRUCTURE]%22%29
            
                    SI ON A UN RESULTAT OK

                    SINON

                        B.1.1) RECHERCHE UNIQUEMENT PAR L AFFILIATION (Partie commentée dans le code car produit beaucoup d'erreurs)
                        
                            ==> REQUETE SOLR EFFECUE SUR LE CORE "ref_structure":

                                Si tutelles renseignees :
                                    q=%28acronym_sci%3A%22[ACRONYME OU NOM]%22+OR+name_sci%3A%22[NOM DE STRUCTURE]%22%29%22AND%22parentAcronym_t:[TUTELLE]%22OR%22parentName_t:[TUTELLE]
                                Sinon :
                                    q=%28acronym_sci%3A%22[ACRONYME OU NOM]%22+OR+name_sci%3A%22[NOM DE STRUCTURE]%22%29
                                &rows=10
                                &wt=phps
                                &fl=docid%2Clabel_s%2Cvalid_s%2Ccountry_s%2CparentName_s%2Ctype_s%2Cvalid_s
                                &sort=valid_s+DESC
        
                                Tant que pas de resultats, selon les cas si on a des info sur le pays ou/et le type plusieurs requetes avec fq different (avec pays AND TYPE puis pays OR type , seulement le pays...)
                                    &fq=country_s%3A[CODE PAYS]+AND+type_s%3A[TYPE STRUCTURE]
        
                            SI DES RESULTATS

                                ON RETOURNE LE RESULTAT AYANT LE MEME TYPE DE STRUCTURE, SINON LE PREMIER RESULTAT

                                    SI STATUS EST AUTRE QUE INCOMING , OK

                                    SINON ON REFAIT LA REQUETE DANS B.1.1) MAIS AVEC DES CHAINES DE CARACTERES SIMILAIRES..

                            SINON

                                SI LA RECHERCHE A ETE TENTE AVEC LES TUTELLES (CONDITION), ON IGNORE LES TUTELLES ET ON REFAIT LA REQUETE DANS B.1.1)
            
        (PAS FINI !) ON REFAIT B.1) POUR (L'INSTANT) AVEC LA PREMIERE AFFILIATION RECHERCHEE !
        (TO DO : OPTIMISE)

             SI RIEN A CE MOMENT:

                   FAIRE A) EN OMETTANT LA CONTRAINTE SUR L'AFFILIATION RECHERCHEE (DONC RECHERCHE AVEC AUTEUR UNIQUEMENT).

                        SI TROUVE OK

                        SINON TERMINE
    			
# Exemples d'appels API

## Exemple cas A)

    curl -XPOST -H 'Content-Type: application/json' 'https://api.archives-ouvertes.fr/ref/affiliation' -d '{
        "lastName_t": "Romary",
        "firstName_t": "Laurent",
        "producedDate_s": "2016-12-07"
    }'

## Exemple cas B)

    curl -XPOST -H 'Content-Type: application/json' 'https://api.archives-ouvertes.fr/ref/affiliation' -d '{
    "lastName_t": "Marchand",
    "firstName_t": "Virginie",
    "producedDate_s": "2018",
    "structure_t": [
        {
            "structName_t": "Epitranscriptomics and RNA Sequencing (EpiRNA-Seq) Core Facility, UMS2008 IBSLor (CNRS-UL)/US40 (INSERM)",
            "structCountry_s": "FR",
            "structType_s": "laboratory"
        },
        {
            "structName_t": "Université de Lorraine",
            "structCountry_s": "FR",
            "structType_s": "institution"
        }
    ]
    }'
